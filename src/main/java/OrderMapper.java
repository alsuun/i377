public class OrderMapper{
	public Order parse(String input){

		input=input.replace("}","");      // asendab loogelise sulu tühikuga
		input=input.replace("{","");
		input.trim();                   // kaotab tühikud
		String[] orders=input.split(":");         // kooloni pealt poolitamine
		String ordernr;
		Order order=new Order();
			//System.out.println(Arrays.asList(iDandName));   //Array convertimine Listiks, sest Arrayd ei saa printida asString
		if(input.contains("id")){
			ordernr=orders[2].replace("\"","").trim();
		}else{
			ordernr=orders[1].replace("\"","").trim();
		}
		/*
		if(input.contains(("orderRows"))){
			//order.setOrderRow(orders[3].replace("\"","").trim());
		}*/

		order.setOrdernr(ordernr);

		return order;
	}
	public String stringify(Order order){
		String result="{ \"";

		result +="id\": \""+ order.getId();
		result+="\", ";
		result += "\"orderNumber\": \""+ order.getOrderNumber()+"\" }";
		return result;
	}
}