import com.fasterxml.jackson.databind.*;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/api/orders")
public class OrderServlet extends HttpServlet {
	private Long orderID=0L;
	private HashMap<Long,Order> orderMap=new HashMap<>();
	private Order newInstance=new Order();

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {
		Order orders=new Order();
	    String input2=request.getParameter("id");
		Long key=Long.parseLong(input2);
	    String responses=new ObjectMapper().writeValueAsString(orderMap.get(key));
	    System.out.println("anname selle vastu-->"+ responses);
	    response.setHeader("Content-Type","application/json");
	    response.getWriter().print(responses);
	    System.out.println("/*******/");
	    System.out.println(newInstance.getOrders(key));
	    System.out.println(orders.getOrders(key));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

	    String input=Util.readStream(req.getInputStream());     //püüame sisendi kinni
	    OrderMapper mapper=new OrderMapper();
	    Order order=mapper.parse(input);
	    //Order newInstance=new Order();
	    orderID=orderID+1L;                                 //tellimuse number suureneb iga päringuga
	    order.setId(orderID);                                 //omistame tellimuse numbri
	    orderMap.put(orderID,order);

	    System.out.println(orderID);

	    newInstance.setOrderList(orderID,order);
	    String orderString=new ObjectMapper().writeValueAsString(order);
	    resp.setHeader("Content-Type", "application/json");
	    resp.getWriter().print(orderString);

	    HttpSession session=req.getSession();
	    session.setAttribute("Orders",orderMap);
	    session.setAttribute("orderIDs",orderID);
	    System.out.println(orderString);
	    System.out.println("/*******/");
	    System.out.println(input);



    }

}