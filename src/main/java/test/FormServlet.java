import com.fasterxml.jackson.databind.*;
import com.sun.xml.internal.org.relaxng.datatype.Datatype;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



@WebServlet("/api/form")
public class FormServlet extends HttpServlet {

	private HashMap<Long,Order> orderMap=new HashMap<>();
	private Order order=new Order();
	private Long orderID;
	private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {
	    System.out.println("doGet");
	    String input=request.getParameter("id");
	    HttpSession session=request.getSession();
	    HashMap<Long, Order> data=(HashMap<Long,Order >)session.getAttribute("Orders");
	    Long key=Long.parseLong(input);

	    System.out.println(orderMap.get(key));
	    String responses=new ObjectMapper().writeValueAsString(orderMap.get(key));
	    response.setHeader("Content-Type","application/json");
	    response.getWriter().print(key.toString());

	    System.out.println();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

	    String orderNumber=req.getParameter("orderNumber");         //võtan sessiooni varasemad päringud
	    HttpSession session=req.getSession();
	    HashMap<Long, Order> data=(HashMap<Long,Order >)session.getAttribute("Orders");
	    orderID=(Long)session.getAttribute("orderIDs");        //vaatan millise IDga on viimane sisestus
		if(orderID==null){
			orderID=1L;
		}else{
		}
	    orderID=+1L;
	    System.out.println(orderNumber);
	    /*Kui sisestusi pole varem olnud siis alustan 1st, ja hakkan suurendama +1 võrra*/
	    System.out.println(orderID);

		order.setOrdernr(orderNumber);
		order.setId(orderID);

	    String orderString=new ObjectMapper().writeValueAsString(order);
	    System.out.println(orderString);
	    orderMap.put(orderID,order);
	    resp.setHeader("Content-Type", "text/html");
	    resp.getWriter().print(orderID.toString());

    }

}