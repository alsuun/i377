
/*
* Date:04.10.2018
* Author: mKalmo
* Source URL:https://bitbucket.org/mkalmo/exservlet/src/master/src/main/java/exservlet/Util.java
*
* */
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class Util {

	public static String readStream(InputStream is) {
		BufferedReader buffer = new BufferedReader(new InputStreamReader(is));

		return buffer.lines().collect(Collectors.joining("\n"));
	}

}