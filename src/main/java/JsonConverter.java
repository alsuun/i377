import java.util.Arrays;

public class JsonConverter {
	public static void main(String[] args) throws Exception{
		/*
		Order order=new Order();
		order.setId(0);
		order.setOrdernr("A123");
		String json=order.toString();
		*/
		String json=" { \"orderNumber\": \"A123\" }\n";
		//Order uus=new OrderMapper().parse(json);
		Order order=new OrderMapper().parse(json);
		String json2=new OrderMapper().stringify(order);

		System.out.println();
		System.out.println(json+" <<<< String to Json");
		System.out.println(json2+"<<<Json to String");
	}
}
