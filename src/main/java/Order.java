import java.util.HashMap;

public class Order {

	private Long numOrders=0L;
	private HashMap<Long,Order> orderList=new HashMap<>();

	public void setOrderList(Long index,Order order){ orderList.put(index,order); }
	public Order getOrderList(Long index){ return orderList.get(index); }

	public HashMap<Long,Order> copyMap(){
		return orderList;

	}

	public Order getOrders(Long key){
		return orderList.get(key);
	}

	/*for troubleshooting*/
	public Integer hashlen(){
		return orderList.size();
	}

	private Long id;
	private String orderNumber;
	//private String orderRow;

	public Long getId(){
		return id; }

	public void setId(Long id){
		this.id=id;}

	public String getOrderNumber(){return orderNumber;}

	public void setOrdernr(String orderNumber){this.orderNumber =orderNumber;}
	/*
	public void setOrderRow(String orderRow){
		this.orderRow=orderRow;
	}

	public String getOrderRow(){
		return orderRow;
	}
	*/
	@Override
	public String toString(){

		return "Order{"+
					   "id:'"+id+
				", orderNumber:"+ orderNumber +
						'}';
	}
}